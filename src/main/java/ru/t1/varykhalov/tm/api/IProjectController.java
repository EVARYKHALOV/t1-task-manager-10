package ru.t1.varykhalov.tm.api;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProjects();

}