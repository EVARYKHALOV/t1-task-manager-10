package ru.t1.varykhalov.tm.api;

import ru.t1.varykhalov.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

}