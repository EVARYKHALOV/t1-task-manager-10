package ru.t1.varykhalov.tm.api;

public interface ITaskController {

    void createTask();

    void showTasks();

    void clearTasks();

}