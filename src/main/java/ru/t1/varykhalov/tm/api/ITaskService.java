package ru.t1.varykhalov.tm.api;

import ru.t1.varykhalov.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task create(String name, String description);

}